#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""GNU/Linux Bot
For managing the GNU/Linux Chat
"""
import sqlite3
import logging
import json
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

LOG = logging.getLogger(__name__)


def valid_user(bot, update, permission):
    """Check if the user who sent the update is allowed to do that"""
    chat = update.effective_chat
    user = update.effective_user
    chat_id = str(chat.id)
    user_id = str(user.id)

    if chat_id in CONFIG.keys():
        allowed_users = CONFIG.get(chat_id).get("users")
        if user_id in allowed_users.keys():
            permissions = allowed_users.get(user_id).get("permissions")
            if permission in permissions:
                return True
    return False


def get_reason(message):
    """Return the reason for the specified command warn/ban"""
    return ' '.join(message.split()[1:])


# Command Handlers
def count_warnings(id):
    """Count the number of warnings a user has given a id"""
    with sqlite3.connect("data.db") as sql:
        cursor = sql.cursor()
        cursor.execute("SELECT count(*) FROM warnings WHERE id=?", (id, ))
        return cursor.fetchall()[0][0]


def reply_to_self(bot, update):
    if update.message.reply_to_message.from_user.id == bot.get_me().id:
        return True
    return False


def warn(bot, update):
    """Add a warning to the message that was replied to"""
    if reply_to_self(bot, update):
        return update.message.reply_text("I cannot warn myself!")
    if not valid_user(bot, update, "warn"):
        return update.message.reply_text(
            "You do not have permissions to use that command here")
    warn_reason = get_reason(update.message.text)
    if not get_reason(update.message.text):
        return update.message.reply_text(
            "You cannot warn without any specific reason!")
    reply_message = update.message.reply_to_message
    bot.delete_message(
        chat_id=update.message.chat.id, message_id=reply_message.message_id)
    # delete the reply message

    bot.delete_message(
        chat_id=update.message.chat.id, message_id=update.message.message_id)
    # delete the message

    with sqlite3.connect("data.db") as sql:
        sql.execute(
            "INSERT INTO warnings(id, reason, content) VALUES (?, ?, ?)",
            (reply_message.from_user.id, warn_reason,
             update.message.reply_to_message.text))
    # append to database

    bot.send_message(
        text=
        "Infraction added, @{username} now has *{count}* infractions\n_Reason: {reason}_".
        format(
            username=reply_message.from_user.username,
            count=count_warnings(reply_message.from_user.id),
            reason=warn_reason),
        parse_mode='markdown',
        chat_id=update.message.chat.id)


def _error(bot, update, error):
    """Log Errors caused by Updates."""
    LOG.warning('Update "%s" caused error "%s"', update, error)


def read_config(config):
    with open(config, 'r') as file:
        return json.loads(file.read())


def create_tables():
    """Create tables required for bot if they don't exist"""
    with sqlite3.connect("data.db") as sql:
        sql.execute("""
                    CREATE TABLE IF NOT EXISTS users(
                        id integer NOT NULL,
                        username text NOT NULL,
                        user_permissions text,
                        active_warnings integer DEFAULT 0,
                        total_warnings integer DEFAULT 0
                    );
                  """)
        sql.execute("""
                    CREATE TABLE IF NOT EXISTS chats(
                        chatid integer NOT NULL,
                        enabled boolean
                    );
                  """)
        sql.execute("""
                    CREATE TABLE IF NOT EXISTS warnings(
                        id integer NOT NULL,
                        reason text,
                        content BLOB NOT NULL,
                        t TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                    );
                  """)


def token():
    """Reads the token from token file"""
    with open("token") as file:
        for line in file:
            return line.splitlines()[0]


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token())
    create_tables()

    # Get the dispatcher to register handlers
    bot = updater.dispatcher

    # on different commands - answer in Telegram
    bot.add_handler(CommandHandler("warn", warn))

    # log all errors
    bot.add_error_handler(_error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    CONFIG = read_config('config.json')
    main()
